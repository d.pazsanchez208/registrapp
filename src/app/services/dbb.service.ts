import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@awesome-cordova-plugins/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class DBBService {

  constructor(private sqlite: SQLite) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS PERSONA(mdl_usuario VARCHAR(20),mdl_nombre VARCHAR(20),mdl_contrasena VARCHAR(20),mdl_usuario_new VARCHAR(20),mdl_contrasena_new VARCHAR(20), mdl_apellido VARCHAR(20),mdl_correo VARCHAR(20))', [])
          .then(() => console.log('DJ: TABLA CREADA OK'))//FSR es para que en el logcat uno puedar filtrar las acciones que uno desee ver
          .catch(e => console.log('DJ: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('DJ: ' + JSON.stringify(e)));
  }

  crearUsuario(mdl_usuario: string, mdl_nombre: string, mdl_contrasena: string, mdl_usuario_new: string, mdl_contrasena_new: string, mdl_apellido: string, mdl_correo: string) {
    this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('INSERT INTO PERSONA VALUES(?,?,?)',[mdl_usuario, mdl_nombre, mdl_contrasena, mdl_usuario_new, mdl_contrasena_new, mdl_apellido, mdl_correo])//(?,?,?)=(PLACE HOLDER) ESTO SE USA PARA QUE NO EJECUTEN INJECIONES DE SQL
          .then(() => console.log('DJ: PERSONA ALMACENADA'))
          .catch(e => console.log('DJ: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('DJ: ' + JSON.stringify(e)));
  }

  obtenerCantidadPersonas() {//siempre se debe poner el return para que las promesas puedan devolver los datos 
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        return db.executeSql('SELECT COUNT(NOMBRE) AS CANTIDAD FROM PERSONA',[])//POR RECOMENDACION DEL PROFE PONER UN ALIAS A LOS COUNT (AS)
          .then((data) => {//(data)es un cursor el nombre puede ser el que uno desee pero es una respuesta del (db.executeSql('SELECT COUNT(NOMBRE) AS CANTIDAD FROM PERSONA',[])) osea la promesa
            return data.rows.item(0).CANTIDAD;
          })
          .catch(e => console.log('DJ: ' + JSON.stringify(e)));
      })
      .catch(e => console.log('DJ: ' + JSON.stringify(e)));
  }
}