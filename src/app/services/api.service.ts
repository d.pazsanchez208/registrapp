import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http'; //importando para poder usar la api

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  ApiProfe: string =// lo que esta en azul es el nombre de la api uno puede poner el nombre que mas le acomode.
  'https://fer-sepulveda.cl/API_PRUEBA_2/api-service.php';
  constructor(private http: HttpClient) { }

  personaListar(){
      return this.http.get(this.ApiProfe).pipe();
    }
  personaAlmacenar(usuario:String,correo:string, contrasena:string, nombre:string,apellido: string){
    return this.http.post(this.ApiProfe,{
      nombreFuncion: 'PersonaAlmacenar',
      parametros:[
        usuario,correo,contrasena,nombre,apellido
      ]
      }).pipe()
    }
}