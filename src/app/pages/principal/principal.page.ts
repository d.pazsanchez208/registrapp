import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.page.html',
  styleUrls: ['./principal.page.scss'],
})
export class PrincipalPage implements OnInit {

  usuario: string ='';
  contrasena: string='';
  mdl_usuario: string='';
  constructor(private router: Router) { }


  ngOnInit() {
    
        let parametros = this.router.getCurrentNavigation();
    
        if(parametros?.extras.state){
          this.usuario = parametros?.extras.state['user'];
          this.contrasena = parametros?.extras.state['pass'];
          
        }
        console.log('mdl_usuario:', this.usuario);
        console.log('mdl_contrasena:', this.contrasena);
  }

  RestCon(){
    let parametros: NavigationExtras = { 
      state: {
        user: this.usuario,
        pass: this.contrasena
      }
    }
    this.router.navigate(['res-cont'], parametros)

  }
}
