import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import{HttpClient} from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
  ApiProfe: string ='https://fer-sepulveda.cl/API_PRUEBA_2/api-service.php';

  mdl_usuario: string ='';
  mdl_contrasena: string='';
  mdl_usuario_creado: string ='';
  mdl_contrasena_creado: string='';

  mensaje: string='';
  
  isAlertOpen = false;
  alertButtons = ['OK'];

  constructor(private router: Router, private apiService: ApiService,private http: HttpClient) {  }

  ngOnInit() {
    let parametros = this.router.getCurrentNavigation();

    if (parametros?.extras.state){
      this.mdl_usuario_creado = parametros?.extras.state['user'] || '';
      this.mdl_contrasena_creado = parametros?.extras.state['pass'] || '';
      console.log('DJ:', this.mdl_usuario);
      console.log('DJ:', this.mdl_contrasena);
      console.log('DJ:', this.mdl_usuario_creado);
      console.log('DJ:', this.mdl_contrasena_creado);

    }
  }

  iniciar() {
    if(this.mdl_usuario == '' || this.mdl_contrasena == ''){
      this.mensaje = 'Ingrese Usuario o Contraseña';
      this.isAlertOpen = true

    } else if (this.mdl_usuario == this.mdl_usuario_creado && this.mdl_contrasena == this.mdl_contrasena_creado) {
      let parametros: NavigationExtras = { 
        state: {
          user: this.mdl_usuario,
          pass: this.mdl_contrasena
        }
      }
      this.router.navigate(['principal'], parametros);

    } else {
      
      this.mensaje='Credenciales incorrectas';
      this.isAlertOpen = true;
      console.log('mdl_usuario:', this.mdl_usuario);
      console.log('mdl_contrasena:', this.mdl_contrasena);
      console.log('mdl_usuario_creado:', this.mdl_usuario_creado);
      console.log('mdl_contrasena_creado:', this.mdl_contrasena_creado);
  
    }
  }
  personaLogin(usuario:string, contrasena:string){
    return this.http.post(this.ApiProfe,{
      nombreFuncion:'UsuarioLogin',
      parametros:[
        usuario,contrasena
      ]
    }).pipe;

  }
  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }

  crearUser() {
    this.router.navigate(['crear-user'])
  }

}
