import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ResContPage } from './res-cont.page';

const routes: Routes = [
  {
    path: '',
    component: ResContPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResContPageRoutingModule {}
