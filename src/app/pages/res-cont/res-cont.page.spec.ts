import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResContPage } from './res-cont.page';

describe('ResContPage', () => {
  let component: ResContPage;
  let fixture: ComponentFixture<ResContPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ResContPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
