import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import{HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-res-cont',
  templateUrl: './res-cont.page.html',
  styleUrls: ['./res-cont.page.scss'],
})
export class ResContPage implements OnInit {
  //Usuario
  usuario: string ='';
  contrasena: string='';
  contrasena_cnfg: string ='';
  //Alerta
  mensaje: string='';
  isAlertOpen = false;
  alertButtons = ['OK'];
  //api
  ApiProfe: string ='https://fer-sepulveda.cl/API_PRUEBA_2/api-service.php';
  

  constructor(private router: Router, private alertController: AlertController, private apiService: ApiService,private http: HttpClient) { 
    this.mensaje = '';
  }
  //Al iniciar
  ngOnInit() {
    let parametros = this.router.getCurrentNavigation();
    
    if(parametros?.extras.state){
      this.usuario = parametros?.extras.state['user'];
      this.contrasena = parametros?.extras.state['pass'];
      
    }
    console.log('mdl_usuario:', this.usuario);
    console.log('mdl_contrasena:', this.contrasena);
  }
  //Al volver al principal
  navegar() {
    let parametros: NavigationExtras = { 
      state: {
        user: this.usuario,
        pass: this.contrasena
      }
    }
    this.router.navigate(['login'], parametros);
  }
  
  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
  

  
  async presentAlert() {
    this.mensaje = 'Ingresa tu contraseña';
    console.log('Usuario:', this.usuario);
    console.log('Contrasena:', this.contrasena);
  
    let parametros: NavigationExtras = {
      state: {
        user: this.usuario,
        pass: this.contrasena
      }
    }
  
    const alert = await this.alertController.create({
      header: 'Ingresa tu contraseña',
      subHeader: 'para confirmar',
      inputs: [
        {
          name: 'contrasenaInput',
          type: 'password'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
        },
        {
          text: 'Aceptar'
        }
      ]
    });
  
    await alert.present();
  
    // Recupera el valor ingresado por el usuario
    const { data } = await alert.onDidDismiss();
    const contrasenaInput = data.values.contrasenaInput;
  
    // Verifica si el valor ingresado por el usuario está vacío
    if (contrasenaInput === '') {
      // Muestra una alerta con el mensaje "el campo no puede ir vacío"
      const emptyFieldAlert = await this.alertController.create({
        header: 'Error',
        message: 'El campo no puede ir vacío',
        buttons: ['OK']
      });
      await emptyFieldAlert.present();
    } else {
      // Actualiza el valor de la variable contrasena
      this.contrasena = contrasenaInput;
  
      // Verifica que el valor de contrasena se haya actualizado correctamente
      console.log('Contrasena actualizada:', this.contrasena);
    }
  }
    personaModificarContrasena(usuario: string, contrasena:string, contrasena_cnfg:string  ){
      return this.http.patch(this.ApiProfe,{
        nombreFuncion:'UsuarioModificarContrasena',
        parametros:[
          usuario,contrasena_cnfg,contrasena
        ]
      }).pipe();
    }
}