import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DBBService } from 'src/app/services/dbb.service';
import {ApiService} from 'src/app/services/api.service';
import { firstValueFrom, lastValueFrom } from 'rxjs';

@Component({
  selector: 'app-crear-user',
  templateUrl: './crear-user.page.html',
  styleUrls: ['./crear-user.page.scss'],
})
export class CrearUserPage implements OnInit {

  mdl_usuario: string ='';
  mdl_nombre: string='';
  mdl_contrasena: string='';
  mdl_usuario_new: string='';
  mdl_contrasena_new: string='';
  mdl_apellido: string='';
  mdl_correo: string='';
  cantidad: number = 0;
  mensaje: string='';
  
  isAlertOpen = false;
  alertButtons = ['OK'];

  lista_personas:any[] = [];
  

  constructor(private router: Router ,private dbService:DBBService, private apiService: ApiService) { }

  ngOnInit() {
    this.dbService.obtenerCantidadPersonas().then(data => {this.cantidad = data;});
    this.personasListar();
    this.personaAlmacenar();
  }

  crearUsuario() {
    
    if(this.mdl_usuario_new === '' && this.mdl_contrasena_new === ''){
      this.mensaje = 'Ingrese credenciales deseadas';
      this.isAlertOpen = true
    }else{
      this.dbService.crearUsuario(this.mdl_usuario, this.mdl_nombre, this.mdl_contrasena, this.mdl_usuario_new, this.mdl_contrasena_new, this.mdl_apellido, this.mdl_correo)
        console.log('DJ: Datos Almacenados')

      this.mensaje='Credenciales Guardadas';
      this.isAlertOpen = true;
      this.router.navigate(['login']); 
    
    }
  }

  setOpen(isOpen: boolean) {
    this.isAlertOpen = isOpen;
  }
  async personasListar(){
  let data = this.apiService.personaListar();
  let respuesta = await lastValueFrom (data);

  let jsonTexto = JSON.stringify(respuesta);
  let json = JSON.parse(jsonTexto);
    for(let x = 0; x < json['result'].length; x++){
      this.lista_personas.push(json['result'][x]);
    } 
  }
  async personaAlmacenar(){
    let data =this.apiService.personaAlmacenar(this.mdl_usuario_new,this.mdl_correo,this.mdl_contrasena_new,this.mdl_nombre,this.mdl_apellido);
    let respuesta = await firstValueFrom(data);
    let jsonTexto = JSON.stringify(respuesta);
    let json = JSON.parse(jsonTexto);

    console.log(jsonTexto);
  }
}
  